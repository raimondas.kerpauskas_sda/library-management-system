import entity.Author;
import entity.Book;
import org.hibernate.Session;
import util.HibernateUtil;

public class Main {
    public static void main (String[] args){
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        Author author = new Author();
        author.setfirstName("Henry");
        author.setlastName("Haines");


        //Add new Book object
        Book book1 = new Book();
        book1.setTitle("A Dance with Dragons");

//        Book book2 = new Book();
//        book2.setTitle("A Storm of Swords");

        //Save Owner
        session.save(author);
        //Save Books
        book1.setAuthors(author);
//        book2.setOwner(owner);
        session.save(book1);
//        session.save(book2);

        session.getTransaction().commit();
        HibernateUtil.shutdown();
    }
}
